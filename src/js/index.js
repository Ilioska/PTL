import '../scss/style.scss';
const imageSetThumb = require.context('../images/thumb', true)
const imageSetFull = require.context('../images/full_size', true)
const articleJson = require('./articleData.json')
let interval = false

// Module for slide images on front page
let number = Math.floor(Math.random()*12)+1;

function slider() {
    number++;
    if (number > 6) number = 1;
    if (number.toString().length === 1) number = '0' + number;
    document.body.style.backgroundImage = "url('" + imageSetFull('./' + number + '.jpg', true) + "')";
}

//window.addEventListener('resize', toggleSlider)

function toggleSlider() {
    if(window.innerWidth > 1040 && !interval) {
        interval = setInterval(slider, 5000)
    }

    if(window.innerWidth <= 1040 && interval) {
        clearInterval(interval)
        interval = false
    }
}

//Module for inject articles contant into HTML
(function(jsonFile) {
    const articleHook = document.getElementById('frontpage-articles')
    for(let i = 0; i < jsonFile.length; i++) {
        const documentCreator = function() {return document.createElement('DIV')}
        const wrapper = documentCreator()
        const container = documentCreator()
        const placeholder = documentCreator()
        const title = documentCreator()
        const content = documentCreator()
        const signature = documentCreator()
        const date = document.createElement('SPAN')
        const button = document.createElement('A')
        wrapper.classList.add('col-4-l', 'col-12-xs')
        container.classList.add('frontpage__information-placeholder--space')
        placeholder.classList.add('frontpage__information-placeholder')
        placeholder.setAttribute('data-infoid', i)
        title.classList.add('frontpage__information-placeholder--title')
        title.textContent = jsonFile[i].title
        content.classList.add('frontpage__information-placeholder--content')
        content.textContent = trimString(jsonFile[i].content)
        signature.classList.add('frontpage__information-placeholder--signature')
        date.textContent = jsonFile[i].date
        button.classList.add('frontpage__link')
        button.textContent = "Więcej"
        button.addEventListener("click", function() {openArticle(i)})
        signature.appendChild(date)
        signature.appendChild(button)
        placeholder.appendChild(title)
        placeholder.appendChild(content)
        placeholder.appendChild(signature)
        container.appendChild(placeholder)
        wrapper.appendChild(container)
        articleHook.appendChild(wrapper)
    }
})(articleJson)

function trimString(string) {
    let maxStringLength = 400;
    const closestSpace = string.substr(maxStringLength, string.length).indexOf(' ')
    maxStringLength = closestSpace <= 0 ? maxStringLength : maxStringLength + closestSpace
    return string.substr(0, maxStringLength).concat('...')
}



//Module which creating gallery thumbs
const galleryFrame = document.querySelector('#galleryFrame')
for(let i = 1; i <=12; i++) {
    const divCol = document.createElement('DIV');
    const divGalleryFrame = document.createElement('DIV')
    const divGallery = document.createElement('DIV')
    divCol.classList.add('col-2-l', 'col-3-m', 'col-4-s', 'col-6-xs')
    divGalleryFrame.classList.add('gallery__frame')
    divGallery.classList.add('gallery__thumb')
    let photoNumber = i
    if (photoNumber.toString().length === 1) photoNumber = '0' + photoNumber;
    divGallery.style.backgroundImage = "url('" + imageSetThumb('./' + photoNumber + '.jpg', true) + "')";
    divGallery.setAttribute('data-photoid', photoNumber);
    divGallery.addEventListener('click', zoom)
    divGalleryFrame.appendChild(divGallery)
    divCol.appendChild(divGalleryFrame)
    galleryFrame.appendChild(divCol)
}

function zoom(event) {
    const id = event.target.getAttribute('data-photoid')
    document.querySelector(".modal").style.display = "flex";
    document.querySelector(".modal__image").style.display = "flex";
    document.querySelector(".modal__image").setAttribute('data-photoid', id);
    document.querySelector(".modal__image").style.backgroundImage = "url('" + imageSetFull('./' + id + '.jpg', true) + "')";
    document.querySelector('.modal__image--close').addEventListener('click', close)
    document.querySelector('.modal__image--right-open').addEventListener('click', next)
    document.querySelector('.modal__image--left-open').addEventListener('click', previous)
}

function close() {
    setTimeout(disappearance, 650);
    document.querySelector(".modal__image").style.animationName = "image_close";
    document.querySelector(".modal").style.animationName = "image_close";
    function disappearance() {
        document.querySelector(".modal").style = "";
        document.querySelector(".modal__image").style.display = "none";
        document.querySelector(".modal__info").style.display = "none";
        document.querySelector(".modal__image").style.animationName = "image_open";
    }
}

function next(event) {
        const target = event.currentTarget.parentNode;
        let id = target.getAttribute('data-photoid')
        if (Number(id) + 1 > 12) id = 0
        let newId = Number(id) + 1;
        if (newId.toString().length === 1) newId = '0' + newId;
        target.style.animationName = `image_close`
        setTimeout(() => {
            target.style.animationName = `image_open`
            target.setAttribute('data-photoid', newId);
            target.style.backgroundImage = "url('" + imageSetFull('./' + newId + '.jpg', true) + "')";
        }, 500)
}

function previous(event) {
    const target = event.currentTarget.parentNode;
    let id = target.getAttribute('data-photoid')
    if (Number(id) - 1 < 1) id = 13
    let newId = Number(id) - 1;
    if (newId.toString().length === 1) newId = '0' + newId;
    target.style.animationName = `image_close`
    setTimeout(() => {
        target.style.animationName = `image_open`
        target.setAttribute('data-photoid', newId);
        target.style.backgroundImage = "url('" + imageSetFull('./' + newId + '.jpg', true) + "')";
    }, 500)
}

// function to open modal for articles
function openArticle(id) {
    document.querySelector(".modal").style.display = "flex";
    document.querySelector(".modal__info").style.display = "flex";
    document.querySelector('.modal__info--close').addEventListener('click', close)
    const articleData = articleJson[id]
    document.getElementById('modal-article-title').textContent = articleData.title
    document.getElementById('modal-article-date').textContent = articleData.date
    document.getElementById('modal-article-content').textContent = articleData.content
}

// Module 'BURGER' for handling navigation at small breakpoint
document.getElementById('burger').addEventListener('click', toggleMenu)

const navBottom = document.querySelector('.nav__bottom')

function toggleMenu() {
    navBottom.classList.toggle('--hidden')
}

// Module for executing navigation links
const navLinks = navBottom.querySelectorAll('li')

for( let i = 0; i < navLinks.length; i++) {
    navLinks[i].addEventListener('click', goToAnchor)
}

function goToAnchor(ev) {
    ev.preventDefault()
    const anchorID = ev.currentTarget.children[0].getAttribute('href').substring(1)
    const offset = window.innerWidth >= 1400 ? 60 : 0
    scrollBy(0, document.getElementById(anchorID).getBoundingClientRect().top - offset)
    toggleMenu()
}

/* toggleSlider() */


// Module for getting accept of RODO
(function() {
    const checkbox = document.getElementById('agree')
    const modal = document.getElementById('rodo-agreement')
    const acceptButton = document.getElementById('rodo-accept')
    const declineButton = document.getElementById('rodo-decline')

    checkbox.addEventListener('change', function() {
        if(this.checked) {
            modal.parentElement.style.display = 'flex'
            modal.style.display = 'flex'
            checkbox.checked = false
        }
    })

    acceptButton.addEventListener('click', function() {
        modal.parentElement.style.display = ''
        modal.style.display = ''
        checkbox.checked = true
    })

    declineButton.addEventListener('click', function() {
        modal.parentElement.style.display = ''
        modal.style.display = ''
    })
})()