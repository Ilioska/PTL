<?php
    function clean_input($input) {
		$input = trim($input);
		$input = stripcslashes($input);
		$input = htmlspecialchars($input);
		return $input;
	}

    $email = clean_input($_POST['email']);
    $message = clean_input($_POST['message']);

	// Przygotowanie treści wiadomości
	$email_text = "
        <p style='padding: 10px; background-color: #03131f; color: #70bdf2; font-weight: bold;'>Wiadomość wysłana automatycznie przy pomocy formularza ze strony PTLowieckie.pl</p>
		<p style='color:#153f65; font-weight: bold; margin: 0; border-bottom: 1px solid #70bdf2;'>Email: </p>
        <p style='background-color: #edf4fe; padding: 10px 15px; margin: 0 0 10px;'>" . $email . "</p>
		<p style='color:#153f65; font-weight: bold; margin: 0; border-bottom: 1px solid #70bdf2;'>Wiadomość: </p>
        <p style='background-color: #edf4fe; padding: 10px 15px; margin: 0 0 10px;'> " . $message . "</p>";

	// Wysyłanie maila za pomocą skrytpu # PHPMailer #
	require_once('./phpmailer/class.phpmailer.php');

	$mailer = new PHPMailer();
    $mailer->CharSet = "UTF-8";
	$mailer->FromName = "Wiadomość PTLowieckie.pl"; #nazwa nadawcy
	$mailer->addAddress("stowarzyszenie@ptlowieckie.pl"); #email odbiorcy
	$mailer->isHTML(true); #format HTML wiadomości
	$mailer->Subject = "Wiadomość wysłana automatycznie przy pomocy formularza ze strony PTLowieckie.pl";
	$mailer->Body = $email_text; #treść wiadomości
    $send = $mailer->send();
	if(!$send) {
		echo('Error');
	} else {
		header('Location: /');
	}
?>
